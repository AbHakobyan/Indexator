﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexator
{
        class Program
        {
            static void Main(string[] args)
            {
                Club footballist = new Club();
                footballist[0] = new Footballist { Fottballist = "Alex", Number = 10 };
                footballist[1] = new Footballist { Fottballist = "Messi", Number = 20 };

                Console.WriteLine(footballist[0].Number);
                Console.WriteLine(footballist[1].Number);
            }
        }

        class Footballist
        {
            public string Fottballist { get; set; }
            public int Number { get; set; }
        }

    class Club
    {
        Footballist[] footballist = new Footballist[11];
        public Footballist this[int index]
        {
            get
            {
                if (index >= 0 && index < footballist.Length)
                {
                    return footballist[index];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (index >= 0 && index < footballist.Length)
                {
                    footballist[index] = value;
                }
            }
        }
    }
}
